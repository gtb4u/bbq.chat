function getScheduler() {
    // private instance variables
    let queue = [], self, timer;

    function schedule(fn, t) {
        timer = setTimeout(function () {
            timer = null;
            fn();
            if (queue.length) {
                let item = queue.shift();
                schedule(item.fn, item.t);
            }
        }, t);
    }

    self = {
        delay: function (fn, t) {
            // if already queuing things or running a timer,
            //   then just add to the queue
            if (queue.length || timer) {
                queue.push({fn: fn, t: t});
            } else {
                // no queue or timer yet, so schedule the timer
                schedule(fn, t);
            }
            return self;
        },
        cancel: function () {
            clearTimeout(timer);
            queue = [];
            return self;
        }
    };

    return self;
}

let currentQueue, instances = {};

export default {
    on(onQueue) {
        currentQueue = onQueue;
        if (!instances.hasOwnProperty(currentQueue)) {
            instances[currentQueue] = getScheduler();
        }
        return this;
    },

    wait(t) {
        instances[currentQueue].delay(() => {}, t);
        return this;
    },

    do(fn) {
        instances[currentQueue].delay(fn, 0);
        return this;
    },

    cancel() {
        instances[currentQueue].cancel();
        return this;
    }

}