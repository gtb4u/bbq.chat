import Vue from 'vue'
import Vuex from 'vuex'

import createMutationsSharer from 'vuex-shared-mutations'
import createPersistedState from 'vuex-persistedstate'
import chat from './services/chat'

Vue.use(Vuex);

import {ADD_MESSAGE, CLEAR_INSTANCE, CLOSE_REVIEWS, CLOSE_WIDGET} from "./const";
import {OPERATOR_CONNECTED} from "./const";
import {OPEN_WIDGET} from "./const";
import {OPEN_REVIEWS} from "./const";
import {MARK_USER_AS_READ} from "./const";
import {MARK_BOT_AS_READ} from "./const";
import {CLOSE_HELP_POPUP} from "./const";
import {OPEN_HELP_POPUP} from "./const";
import {IS_LOADED} from "./const";
import {CLEAR_HISTORY} from "./const";
import {NEW_INSTANCE} from "./const";
import {DATA_COLLECTED} from "./const";
import {CURRENT_STEP} from "./const";
import {COLLECTED_DATA_STORE} from "./const";
import {CURRENT_RESPONSE} from "./const";
import utils from "./utils";

const plugins=[];
plugins.push(createMutationsSharer({
    predicate: [
        ADD_MESSAGE,
        CLOSE_WIDGET,
        OPERATOR_CONNECTED,
        OPEN_WIDGET,
        MARK_USER_AS_READ,
        MARK_BOT_AS_READ,
        CLOSE_HELP_POPUP,
        OPEN_HELP_POPUP,
        CLEAR_HISTORY,
        NEW_INSTANCE,
        DATA_COLLECTED,
        CLEAR_INSTANCE,
        CLOSE_REVIEWS,
        OPEN_REVIEWS,
        CURRENT_STEP,
        COLLECTED_DATA_STORE,
        CURRENT_RESPONSE
    ]
}));

if(utils.get().test === undefined) {
    plugins.push(createPersistedState());
}

export default new Vuex.Store({
    state: {
        lastOpened : utils.currentTime(),
        messages: [],
        instancesCount : 0,
        widget: {
            isClosed: true,
            position: window.chat_bq_conf.chat.position,
            autoRun: window.chat_bq_conf.chat.activations.autorun,
            loaded : false
        },
        popups: {
            help: {
                isVisible: false,
                enabled : window.chat_bq_conf.chat.activations.cold_clients,
                wasViewed : false
            }
        },
        operator: {
            isConnected: false,
            avatar: window.chat_bq_conf.operator.avatar,
            name: window.chat_bq_conf.operator.name,
            job: window.chat_bq_conf.operator.name
        },
        isNotCollectedMinimalData : true,
        canShowReviews: false,
        currentStep : 'no-data',
        collectedData : {},
        currentResponse: {},
        startWriteFirstMessage : false,
        startWriteSecondMessage : false
    },
    mutations: {
        filterMessages(state) {
            let firstMessage = true;
            let secondMessage = true;
            state.messages = state.messages.filter(function(item){
                if(item.category == 'first_message'){
                    firstMessage = false;
                }
                if(item.category == 'second_message'){
                    secondMessage = false;
                }
                return item.category == 'unknown' || firstMessage || secondMessage;
            });
        },
        start_write_first_message(state){
            state.startWriteFirstMessage = true;
        },
        end_write_first_message(state){
            state.startWriteFirstMessage = false;
        },
        start_write_second_message(state){
            state.startWriteSecondMessage = true;
        },
        end_write_second_message(state){
            state.startWriteSecondMessage = false;
        },
        [CURRENT_STEP](state, step) {
            state.lastOpened = utils.currentTime();
            state.currentStep = step;
        },
        [ADD_MESSAGE](state, message) {
            state.lastOpened = utils.currentTime();
            state.messages.push(message);
            chat.scrollDown();
        },
        [COLLECTED_DATA_STORE](state, collectedData) {
            state.lastOpened = utils.currentTime();
            state.collectedData = collectedData;
        },
        [CURRENT_RESPONSE](state, response) {
            state.lastOpened = utils.currentTime();
            state.currentResponse = response;
        },
        [IS_LOADED](state) {
            state.lastOpened = utils.currentTime();
            state.widget.loaded = true;
        },
        [OPERATOR_CONNECTED](state) {
            state.lastOpened = utils.currentTime();
            state.operator.isConnected = true;
        },
        [CLOSE_WIDGET](state) {
            state.lastOpened = utils.currentTime();
            state.widget.isClosed = true;
        },
        [CLOSE_REVIEWS](state) {
            state.lastOpened = utils.currentTime();
            state.canShowReviews = false;
        },
        [OPEN_REVIEWS](state) {
            state.lastOpened = utils.currentTime();
            state.canShowReviews = true;
        },
        [OPEN_WIDGET](state) {
            state.lastOpened = utils.currentTime();
            state.widget.isClosed = false;
        },
        [MARK_USER_AS_READ](state) {
            state.popups.help.wasViewed = true;
            state.lastOpened = utils.currentTime();
            state.messages.forEach((item) => {
                if (item.author === 'bot') {
                    item.has_read = true;
                }
            });
        },
        [MARK_BOT_AS_READ](state) {
            state.lastOpened = utils.currentTime();
            state.messages.forEach((item) => {
                if (item.author === 'user') {
                    item.has_read = true;
                }
            });
            chat.operatorHasRead();
        },
        [CLOSE_HELP_POPUP](state) {
            state.lastOpened = utils.currentTime();
            state.popups.help.isVisible = false;
        },
        [OPEN_HELP_POPUP](state) {
            state.lastOpened = utils.currentTime();
            if (state.popups.help.enabled) {
                if(state.popups.help.wasViewed === false) {
                    state.popups.help.isVisible = true;
                    state.popups.help.wasViewed = true;
                }
            }
        },
        [NEW_INSTANCE](state){
            if(utils.passedMoreThen20Minutes(state.lastOpened)){
                state.lastOpened = utils.currentTime();
                state.messages = [];
                state.widget.isClosed = true;
                state.widget.autoRun = window.chat_bq_conf.chat.activations.autorun;
                state.widget.position = window.chat_bq_conf.chat.position;
                state.widget.loaded = false;
                state.popups.help.isVisible = false;
                state.popups.help.wasViewed = false;
                state.operator.isConnected = false;
                state.operator.avatar = window.chat_bq_conf.operator.avatar;
                state.operator.name = window.chat_bq_conf.operator.name;
                state.operator.job = window.chat_bq_conf.operator.job;
                state.instancesCount = 0;
                state.isNotCollectedMinimalData = true;
                state.currentStep = 'no-data';
                state.collectedData = {};
                state.currentResponse = {};
            }

            state.lastOpened = utils.currentTime();
        },
        [CLEAR_INSTANCE](state){
            if(utils.passedMoreThen20Minutes(state.lastOpened)){
                state.lastOpened = utils.currentTime();
                state.messages = [];
                state.widget.isClosed = true;
                state.widget.autoRun = window.chat_bq_conf.chat.activations.autorun;
                state.widget.position = window.chat_bq_conf.chat.position;
                state.widget.loaded = false;
                state.popups.help.isVisible = false;
                state.popups.help.wasViewed = false;
                state.operator.isConnected = false;
                state.operator.avatar = window.chat_bq_conf.operator.avatar;
                state.operator.name = window.chat_bq_conf.operator.name;
                state.operator.job = window.chat_bq_conf.operator.job;
                state.instancesCount = 0;
                state.isNotCollectedMinimalData = true;
                state.currentStep = 'no-data';
                state.collectedData = {};
                state.currentResponse = {};
            }

            state.lastOpened = utils.currentTime();
        },
        [DATA_COLLECTED](state){
            state.lastOpened = utils.currentTime();
            state.isNotCollectedMinimalData = false;
        },
        [CLEAR_HISTORY](state){
            if(utils.passedMoreThen20Minutes(state.lastOpened)){
                state.lastOpened = utils.currentTime();
                state.messages = [];
                state.widget.isClosed = true;
                state.widget.autoRun = window.chat_bq_conf.chat.activations.autorun;
                state.widget.position = window.chat_bq_conf.chat.position;
                state.widget.loaded = false;
                state.popups.help.isVisible = false;
                state.popups.help.wasViewed = false;
                state.operator.isConnected = false;
                state.operator.avatar = window.chat_bq_conf.operator.avatar;
                state.operator.name = window.chat_bq_conf.operator.name;
                state.operator.job = window.chat_bq_conf.operator.job;
                state.instancesCount = 0;
                state.isNotCollectedMinimalData = true;
                state.currentStep = 'no-data';
                state.collectedData = {};
                state.currentResponse = {};
            }

            state.lastOpened = utils.currentTime();
        },
        reset_chat_minutes(state){
            if(utils.passedMoreThen20Minutes(state.lastOpened)){
                state.lastOpened = utils.currentTime();
                state.messages = [];
                state.widget.isClosed = true;
                state.widget.autoRun = window.chat_bq_conf.chat.activations.autorun;
                state.widget.position = window.chat_bq_conf.chat.position;
                state.widget.loaded = false;
                state.popups.help.isVisible = false;
                state.popups.help.wasViewed = false;
                state.operator.isConnected = false;
                state.operator.avatar = window.chat_bq_conf.operator.avatar;
                state.operator.name = window.chat_bq_conf.operator.name;
                state.operator.job = window.chat_bq_conf.operator.job;
                state.instancesCount = 0;
                state.isNotCollectedMinimalData = true;
                state.currentStep = 'no-data';
                state.collectedData = {};
                state.currentResponse = {};
            }
            state.lastOpened = utils.currentTime();
        }
    },
    actions: {
        [ADD_MESSAGE]({commit}, message) {
            commit(ADD_MESSAGE, message);
        },
        [CURRENT_STEP]({commit}, step) {
            commit(CURRENT_STEP, step);
        },

        [OPERATOR_CONNECTED]({commit}) {
            commit(OPERATOR_CONNECTED);
        },

        [CLOSE_WIDGET]({commit}) {
            commit(CLOSE_WIDGET);
        },

        [OPEN_WIDGET]({commit}) {
            commit(OPEN_WIDGET);
        },

        [MARK_USER_AS_READ]({commit}) {
            commit(MARK_USER_AS_READ);
            chat.operatorHasRead();
        },

        [MARK_BOT_AS_READ]({commit}) {
            commit(MARK_BOT_AS_READ);
        },

        [CLOSE_HELP_POPUP]({commit}) {
            commit(CLOSE_HELP_POPUP);
        },

        [OPEN_HELP_POPUP]({commit}) {
            commit(OPEN_HELP_POPUP);
        },
        [CLEAR_HISTORY]({commit}){
            commit(CLEAR_HISTORY);
        },
        [CLEAR_INSTANCE]({commit}){
            commit(CLEAR_INSTANCE);
        }
    },
    getters: {
        getGreetings: state => {
            let firstMessage = 0;
            let secondMessage = 0;
            let messages = state.messages.filter(function(item){
                let filterBool = item.category == 'unknown' || firstMessage == 1 || secondMessage == 1 || item.author == 'user';
                if(item.category == 'first_message'){
                    firstMessage++;
                }
                if(item.category == 'second_message'){
                    secondMessage++;
                }
                return filterBool;
            });

            return messages.filter(item => {
                return item.isGreeting
            });
        },

        unreadCountMessages: state => {
            let firstMessage = 0;
            let secondMessage = 0;
            let messages = state.messages.filter(function(item){
                let filterBool = item.category == 'unknown' || firstMessage == 1 || secondMessage == 1 || item.author == 'user';
                if(item.category == 'first_message'){
                    firstMessage++;
                }
                if(item.category == 'second_message'){
                    secondMessage++;
                }
                return filterBool;
            });

            return messages.filter(item => {
                return item.has_read === false && item.author === 'bot'
            }).length;
        },

        isChatClosed: state => {
            return state.widget.isClosed;
        },

        isLeftPosition: state => {
            return window.chat_bq_conf.chat.position === "left";
        },

        messages: state => {
            let firstMessage = 0;
            let secondMessage = 0;
            return state.messages.filter(function(item){
                let filterBool = item.category == 'unknown' || firstMessage == 1 || secondMessage == 1 || item.author == 'user';
                if(item.category == 'first_message'){
                    firstMessage++;
                }
                if(item.category == 'second_message'){
                    secondMessage++;
                }
                return filterBool;
            });
        },
        operatorIsConnected: state => {
            return state.operator.isConnected;
        },
        isWidgetClosed: state => {
            return state.widget.isClosed;
        },
        isAutoRun: state => {
            return state.widget.autoRun;
        },
        isHelpVisible: state => {
            return state.popups.help.isVisible && state.widget.isClosed;
        },
        operator: state => {
            return window.chat_bq_conf.operator;
        },
        isNotCollectedMinimalData: state =>{
            return state.isNotCollectedMinimalData;
        },
        isLoaded: state =>{
            return state.widget.loaded;
        },
        canShowReviews: state => {
            return state.canShowReviews;
        },
        currentStep: state => {
            return state.currentStep;
        },
        collectedData: state => {
            return state.collectedData;
        },
        currentResponse: state => {
            return state.currentResponse;
        },
        isFirstMessage: state => {
            return state.startWriteFirstMessage
        },
        isSecondMessage: state => {
            return state.startWriteSecondMessage
        }
    },
    plugins: plugins
})
