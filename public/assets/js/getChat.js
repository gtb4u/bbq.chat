;(function () {
    window.chat_conf = {
        category: window.chat_bq_conf.additional.CampaignID || '39cd64bf',
        intervals: {
            INTERVAL_OPERATOR_WRITE: 1,
            INTERVAL_OPERATOR_WROTE: 1,
            INTERVAL_USER_HAS_READ: 5,
            INTERVAL_OPEN_HELP_POPUP: 60,
            // INTERVAL_OPEN_HELP_POPUP: 5,
            INTERVAL_OPERATOR_CONNECT: 4,
            INTERVAL_BEFORE_CONNECT: parseInt(window.chat_bq_conf.chat.activations.timeout),//10
            INTERVAL_REMINDER_RETURN_WAIT: 2,
            INTERVAL_REMINDER_RETURN_SHOW: 3
        }
    };

    var addStyle = function (link) {
        var style = document.createElement('link');
        style.type = 'text/css';
        style.rel = 'stylesheet';
        style.href = link;
        document.getElementsByTagName('head')[0].appendChild(style);
    };

    var addScript = function (src) {
        var chat_bq = document.createElement('script');
        chat_bq.setAttribute('src', src);
        chat_bq.setAttribute('rel', 'preload');
        document.body.appendChild(chat_bq);
    };

    var addBlock = function (id) {
        var div = document.createElement('div');
        div.setAttribute("id", id);
        document.body.appendChild(div);
    };

    var inlineStyle = function (style) {
        var styleDom = document.createElement('style');
        styleDom.innerHTML = '.chat-bq.closed-bq .greeting-bq,\n' +
            '.chat-bq .header-bq,\n' +
            '.help-popup-bq .help-wrap-bq .btns-bq .yes-bq,\n' +
            '.ms-bq > div,\n' +
            '.form-bq:not(.msg-form-bq)\n' +
            '{\n' +
            'background-color: ' + style.color + ';\n' +
            '}\n' +
            '.pic-load-bq,\n' +
            '.form-bq .f-svg path,\n' +
            '.form-bq .f-svg svg,\n' +
            '.wrap-form-bq.focused-bq .current-bq .f-svg:after,\n' +
            '.loader-bq .spin-bq{\n' +
            'color: ' + style.color + ';\n' +
            'fill: ' + style.color + ';\n' +
            '}\n' +
            '.chat-bq.closed-bq .greeting-bq:before {\n' +
            'border-right: 6px solid ' + style.color + ';\n' +
            '}\n' +
            '.help-popup-bq .help-wrap-bq .btns-bq .yes-bq:after {' +
            'border: 7px solid rgba(' + hexToRgbWithAlpha(style.color, 0.15) + ');' +
            '}' +
            '.help-popup-bq .help-wrap-bq .btns-bq .yes-bq:before {' +
            'border: 7px solid rgba(' + hexToRgbWithAlpha(style.color, 0.4) + ');' +
            '}' +
            '.wrap-form-bq.focused-bq .current-bq input:not(.submit-bq),.wrap-form-bq.focused-bq .current-bq .f-svg path {\n' +
            'border-color: ' + style.color + ' !important;\n' +
            'fill: ' + style.color + ';\n' +
            '}' +
            '.chat-bq *::-moz-selection {\n' +
            '    background: '+style.color+';\n' +
            '}\n' +
            '\n' +
            '.chat-bq *::selection {\n' +
            '    background: '+style.color+';\n' +
            '}';

        document.getElementsByTagName('head')[0].appendChild(styleDom);
    };

    var hexToRgbWithAlpha = function (hex, alpha) {
        hex = hex.replace('#', '');
        var bigint = parseInt(hex, 16);
        var r = (bigint >> 16) & 255;
        var g = (bigint >> 8) & 255;
        var b = bigint & 255;

        return r + "," + g + "," + b + "," + alpha;
    };

    var getParams = function (url) {
        var params = {};
        var parser = document.createElement('a');
        parser.href = url;
        var query = parser.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            params[pair[0]] = decodeURIComponent(pair[1]);
        }
        return params;
    };

    window.referrerParams = getParams(window.location.href);
    if (window.referrerParams.a_aid !== undefined) {
        let myObject = JSON.parse('{"a_aid":"' + window.referrerParams.a_aid + '","a_bid":"' + window.referrerParams.a_bid + '"}');
        let e = 'Thu Nov 26 2020 15:44:38';
        document.cookie = 'bbqSaleAfs=' + JSON.stringify(myObject) + ';expires=' + e;
    }

    let result = document.cookie.match(new RegExp('bbqSaleAfs=([^;]+)'));
    if(result) {
        result = JSON.parse(result[1]);
        if (result.a_aid !== undefined && result.a_aid !== '') {
            window.referrerParams = result;
        }
    }

    var addTrackingBlock = function () {
        let trackImage = document.createElement('img');
        trackImage.src = 'https://webmaster.bbq.sale/scripts/zhki57n4awm?a_aid=' + window.chat_bq_conf.additional.AffiliateID + '&a_bid=' + (window.chat_bq_conf.additional.bid || window.chat_bq_conf.additional.BannerID);
        trackImage.style.border = 0;
        trackImage.style.width = 1 + 'px';
        trackImage.style.height = 1 + 'px';
        document.body.appendChild(trackImage);

        let scriptTrack = window.document.createElement( 'script' );
        scriptTrack.id = 'pap_x2s6df8d';
        scriptTrack.src = 'https://webmaster.bbq.sale/scripts/trackjs.js';
        document.body.appendChild(scriptTrack);

        let inlineTrack = document.createElement('script');
        inlineTrack.innerHTML =
        '        PostAffTracker.setAccountId(\'' + (window.chat_bq_conf.additional.AffiliateID || 'default1') + '\');\n' +
        '        PostAffTracker.disableTrackingMethod(\'F\');\n' +
        '        PostAffTracker.setCookieDomain(window.location.hostname);\n' +
        '        var AffiliateID = \'' + window.chat_bq_conf.additional.AffiliateID + '\';\n' +
        '        var BannerID = \'' + (window.chat_bq_conf.additional.bid || window.chat_bq_conf.additional.BannerID) + '\';\n' +
        '        var CampaignID = \'' + window.chat_bq_conf.additional.CampaignID + '\';\n' +
        '        var Channel = \'' + (window.chat_bq_conf.additional.Channel || window.chat_bq_conf.additional.channel || '') + '\';\n' +
        '        var Data1 = \'' + (window.chat_bq_conf.additional.Data1 || window.chat_bq_conf.additional.data1 || '') + '\';\n' +
        '        var Data2 = \'' + (window.chat_bq_conf.additional.Data2 || window.chat_bq_conf.additional.data2 || '') + '\';\n' +
        '        var Data3 = \'' + (window.chat_bq_conf.additional.Data3 || window.chat_bq_conf.additional.data3 || '') + '\';\n' +
        '        var Data4 = \'' + (window.chat_bq_conf.additional.Data4 || window.chat_bq_conf.additional.data4 || '') + '\';\n' +
        '        var Data5 = \'' + (window.chat_bq_conf.additional.Data5 || window.chat_bq_conf.additional.data5 || '') + '\';\n' +
        '        try {\n' +
        '            PostAffTracker.track();\n' +
        '        } catch (err) { }\n';
        setTimeout(function () {
            document.body.appendChild(inlineTrack);
        }, 800);

        let yandexMetrica = document.createElement('script');
        yandexMetrica.innerHTML = '(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(53445169, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true });';

        setTimeout(function () {
            document.body.appendChild(yandexMetrica);
        }, 800);
    };

    addBlock('app');

    // const SERVER = 'https://webmaster.bbq.sale/forms/chat';
    const SERVER = '.';

    window.SERVER = SERVER;
    inlineStyle({
        color: window.chat_bq_conf.chat.color
    });

    addStyle('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900');
    addStyle(SERVER + '/assets/style.css');

    setTimeout(function () {
        addTrackingBlock();
    }, 200);

    addScript(SERVER + '/assets/js/bundle.js');
    // addScript(SERVER + '/js/chat.js');
    setTimeout(function () {
        addScript(SERVER + '/app.js');
    }, 100);
})();