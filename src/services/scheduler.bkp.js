// private instance variables
let timers = [], queues = [], currentTimer;

function schedule(fn, t) {
    timers[currentTimer] = setTimeout(function () {
        timers[currentTimer] = null;
        fn();
        if (queues[currentTimer].length) {
            let item = queues[currentTimer].shift();
            schedule(item.fn, item.t);
        }
    }, t);
}

export default {
    delay(fn, t) {
        // if already queuing things or running a timer,
        //   then just add to the queue
        if (queues[currentTimer].length || timers[currentTimer]) {
            queues[currentTimer].push({fn: fn, t: t});
        } else {
            // no queue or timer yet, so schedule the timer
            schedule(fn, t);
        }
        return this;
    },
    cancel() {
        clearTimeout(timers[currentTimer]);
        timers[currentTimer] = null;
        queues[currentTimer] = [];
        return this;
    },
    wait(t) {
        return this.delay(() => {}, t);
    },
    do(fn) {
        return this.delay(fn, 0);
    },
    on(queue) {
        currentTimer = queue;
        if (queues[currentTimer] === undefined) {
            queues[currentTimer] = [];
        }
        if (timers[currentTimer] === undefined) {
            timers[currentTimer] = null;
        }
        return this;
    }
}