import {ApiAiClient, ApiAiConstants} from "api-ai-javascript";
import {getAgent, uuid} from "../const";
import {AGENT_BUDGET, AGENT_DATE, AGENT_USED} from "../const";


var navigator_info = window.navigator;
var screen_info = window.screen;
var uid = navigator_info.mimeTypes.length;
uid += navigator_info.userAgent.replace(/\D+/g, '');
uid += navigator_info.plugins.length;
uid += screen_info.height || '';
uid += screen_info.width || '';
uid += screen_info.pixelDepth || '';


// const sessionId = uuid();
const sessionId = uid + 10;
const lang = ApiAiConstants.AVAILABLE_LANGUAGES.RU;

const client = new ApiAiClient({accessToken: getAgent(AGENT_USED), lang: lang, sessionId: sessionId + '1'});

let userStack = [];

export default {
    push(message) {
        userStack.push(message);
    },

    get(){
      return userStack.join("\n");
    },

    clear() {
        userStack = [];
    },

    async request(currentStep, collectedData) {
        if(currentStep === 'name'){

            if (window.yandexGoals.Fill_Chat_Name) {
                window.ym(53445169, 'reachGoal', 'Fill_Chat_Name');
                window.yandexGoals.Fill_Chat_Name = false;
            }

            let returnObject = {
                result: {
                    parameters:{
                        name : userStack.join("\n"),
                        location : collectedData.location,
                        budget: collectedData.budget,
                        date: collectedData.date,
                        email: collectedData.email,
                        'phone-number': collectedData['phone-number']
                    },
                    fulfillment:{
                        speech: 'Последний вопрос, '+userStack.join("")+'! Где планируете делать ремонт?'
                    }
                }
            };
            if(window.chat_bq_conf.additional.CampaignID == '0ebfbec0'){//repair
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
                returnObject.result.parameters['type_of_object'] = collectedData.type_of_object;
                returnObject.result.parameters['type_of_repair'] = collectedData.type_of_repair;
            } else if(window.chat_bq_conf.additional.CampaignID == '39cd64bf'){//building
                returnObject.result.parameters['type_of_building'] = collectedData.type_of_building;
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
                returnObject.result.parameters['type_of_object'] = collectedData.type_of_object;
            }
            else if(window.chat_bq_conf.additional.CampaignID == '77f6e16f'){//design
                returnObject.result.parameters['type_of_service'] = collectedData.type_of_service;
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
            }
            else if(window.chat_bq_conf.additional.CampaignID == '9f266731'){//landscape
                returnObject.result.parameters['type_of_design'] = collectedData.type_of_design;
                returnObject.result.parameters['type_of_work'] = collectedData.type_of_work;
            }
            return returnObject;
        }
        if(currentStep === 'phone-number'){
            if (window.yandexGoals.Fill_Chat_Tel) {
                window.ym(53445169, 'reachGoal', 'Fill_Chat_Tel');
                window.yandexGoals.Fill_Chat_Tel = false;
            }

            let returnObject = {
                result: {
                    parameters:{
                        location : collectedData.location,
                        budget: collectedData.budget,
                        date: collectedData.date,
                        email: collectedData.email,
                        name: collectedData.name,
                        'phone-number': userStack.join("\n")
                    },
                    fulfillment:{
                        speech: 'Скажите, как я могу к Вам обращаться?'
                    }
                }
            };
            if(window.chat_bq_conf.additional.CampaignID == '0ebfbec0'){//repair
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
                returnObject.result.parameters['type_of_object'] = collectedData.type_of_object;
                returnObject.result.parameters['type_of_repair'] = collectedData.type_of_repair;
            } else if(window.chat_bq_conf.additional.CampaignID == '39cd64bf'){//building
                returnObject.result.parameters['type_of_building'] = collectedData.type_of_building;
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
                returnObject.result.parameters['type_of_object'] = collectedData.type_of_object;
            }
            else if(window.chat_bq_conf.additional.CampaignID == '77f6e16f'){//design
                returnObject.result.parameters['type_of_service'] = collectedData.type_of_service;
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
            }
            else if(window.chat_bq_conf.additional.CampaignID == '9f266731'){//landscape
                returnObject.result.parameters['type_of_design'] = collectedData.type_of_design;
                returnObject.result.parameters['type_of_work'] = collectedData.type_of_work;
            }
            return returnObject;
        }

        if(currentStep === 'location'){
            if (window.yandexGoals.Fill_Chat_Geo) {
                window.ym(53445169, 'reachGoal', 'Fill_Chat_Geo');
                window.yandexGoals.Fill_Chat_Geo = false;
            }
            let returnObject = {
                result: {
                    parameters:{
                        location : userStack.join("\n"),
                        budget: collectedData.budget,
                        date: collectedData.date,
                        email: collectedData.email,
                        name: collectedData.name,
                        'phone-number': collectedData['phone-number']
                    },
                    fulfillment: {
                        speech : collectedData.name + ', благодарю за информацию! Ожидайте звонка в ближайшее время!'
                    }
                }
            };
            if(window.chat_bq_conf.additional.CampaignID == '0ebfbec0'){//repair
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
                returnObject.result.parameters['type_of_object'] = collectedData.type_of_object;
                returnObject.result.parameters['type_of_repair'] = collectedData.type_of_repair;
            } else if(window.chat_bq_conf.additional.CampaignID == '39cd64bf'){//building
                returnObject.result.parameters['type_of_building'] = collectedData.type_of_building;
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
                returnObject.result.parameters['type_of_object'] = collectedData.type_of_object;
            }
            else if(window.chat_bq_conf.additional.CampaignID == '77f6e16f'){//design
                returnObject.result.parameters['type_of_service'] = collectedData.type_of_service;
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
            }
            else if(window.chat_bq_conf.additional.CampaignID == '9f266731'){//landscape
                returnObject.result.parameters['type_of_design'] = collectedData.type_of_design;
                returnObject.result.parameters['type_of_work'] = collectedData.type_of_work;
            }
            return returnObject;
        }

        if(currentStep === 'end'){
            let returnObject = {
                result: {
                    parameters:{
                        location :collectedData.location,
                        budget: collectedData.budget,
                        date: collectedData.date,
                        email: collectedData.email,
                        name: collectedData.name,
                        'phone-number': collectedData['phone-number']
                    },
                    fulfillment: {
                        speech : collectedData.name + ', благодарю за информацию! Ожидайте звонка в ближайшее время!'
                    }
                }
            };
            if(window.chat_bq_conf.additional.CampaignID == '0ebfbec0'){//repair
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
                returnObject.result.parameters['type_of_object'] = collectedData.type_of_object;
                returnObject.result.parameters['type_of_repair'] = collectedData.type_of_repair;
            } else if(window.chat_bq_conf.additional.CampaignID == '39cd64bf'){//building
                returnObject.result.parameters['type_of_building'] = collectedData.type_of_building;
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
                returnObject.result.parameters['type_of_object'] = collectedData.type_of_object;
            }
            else if(window.chat_bq_conf.additional.CampaignID == '77f6e16f'){//design
                returnObject.result.parameters['type_of_service'] = collectedData.type_of_service;
                returnObject.result.parameters['type_of_flat'] = collectedData.type_of_flat;
            }
            else if(window.chat_bq_conf.additional.CampaignID == '9f266731'){//landscape
                returnObject.result.parameters['type_of_design'] = collectedData.type_of_design;
                returnObject.result.parameters['type_of_work'] = collectedData.type_of_work;
            }
            return returnObject;
        }

        let dialogFlowResponse = {};

        let message = userStack.join('.').replace("\n", "");
        let splitMessage = message.match(/.{1,255}(\s|$)/g);
        for (let i = 0, arraySize = splitMessage.length; i < arraySize; i++) {
            let currentMessage = splitMessage[i];
            dialogFlowResponse = await client.textRequest(currentMessage);
        }
        if (window.yandexGoals.Fill_Chat_Desc) {
            window.ym(53445169, 'reachGoal', 'Fill_Chat_Desc');
            window.yandexGoals.Fill_Chat_Desc = false;
        }
        return dialogFlowResponse;
    }
}