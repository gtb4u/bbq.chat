export const ONE_SECOND = 1000;

export const ADD_MESSAGE = 'add_message';

export const OPERATOR_CONNECTED = 'operator_connected';

export const CLOSE_WIDGET = 'close_widget';
export const OPEN_WIDGET = 'open_widget';

export const CLOSE_REVIEWS = 'close_reviews';
export const OPEN_REVIEWS = 'open_reviews';

export const MARK_USER_AS_READ = 'mark_user_as_read';
export const MARK_BOT_AS_READ = 'mark_bot_as_read';

export const CLOSE_HELP_POPUP = 'close_help_popup';
export const OPEN_HELP_POPUP = 'open_help_popup';

export const IS_LOADED = 'is_loaded';

export const CLEAR_HISTORY = 'clear_history';
export const CLEAR_INSTANCE = 'clear_instance';
export const DATA_COLLECTED = 'data_collected';
export const CURRENT_STEP = 'current_step';
export const COLLECTED_DATA_STORE = 'collected_data_store';
export const CURRENT_RESPONSE = 'current_response';

export const NEW_INSTANCE = 'new_instance';

export const HOST_API = 'https://wrap.pap.bbq.sale/pap/process_form_data';

const AGENT_REPAIR_KEY = '55038ea2ebe54a519ef1644430baf564';
const AGENT_BUILDING_KEY = '6ba3e7dccf4e4d42aa8fed0b27c394c4';
const AGENT_DESIGN_KEY = 'dfa102a70f1d4b4d98cf4ee4697fc404';
const AGENT_LANDSCAPE_KEY = '3531ebccef0a4890b292a544456378df';
const AGENT_DATE_KEY = '9df9f45cffe74daa8b2aed704c4ccc59';
const AGENT_BUDGET_KEY = 'f3af2bfef4634347baf586d1ef29632e';

export const AGENT_REPAIR = 'repair';
export const AGENT_BUILDING = 'building';
export const AGENT_DESIGN = 'design';
export const AGENT_LANDSCAPE = 'landscape';
export const AGENT_DATE = 'date';
export const AGENT_BUDGET = 'budget';

export const AGENTS = {
    '0ebfbec0': AGENT_REPAIR_KEY,//repair
    '39cd64bf': AGENT_BUILDING_KEY,//building/construction
    '77f6e16f': AGENT_DESIGN_KEY,//design
    '9f266731': AGENT_LANDSCAPE_KEY,//landscape
    'budget' : AGENT_BUDGET_KEY,
    'date' : AGENT_DATE_KEY
};

export const AGENT_USED = window.chat_bq_conf.additional.CampaignID || '39cd64bf';

export function getAgent(agentCategory) {
    return AGENTS[agentCategory];
}

export function uuid() {
    return '_' + Math.random().toString(36).substr(2, 9);
}

export const INTERVAL_OPERATOR_WRITE = window.chat_conf.intervals.INTERVAL_OPERATOR_WRITE;
export const INTERVAL_OPERATOR_WROTE = window.chat_conf.intervals.INTERVAL_OPERATOR_WROTE;
export const INTERVAL_USER_HAS_READ = window.chat_conf.intervals.INTERVAL_USER_HAS_READ;
export const INTERVAL_OPEN_HELP_POPUP = window.chat_conf.intervals.INTERVAL_OPEN_HELP_POPUP;
export const INTERVAL_OPERATOR_CONNECT = window.chat_conf.intervals.INTERVAL_OPERATOR_CONNECT;
export const INTERVAL_BEFORE_CONNECT = window.chat_conf.intervals.INTERVAL_BEFORE_CONNECT;
export const INTERVAL_REMINDER_RETURN_WAIT = window.chat_conf.intervals.INTERVAL_REMINDER_RETURN_WAIT;
export const INTERVAL_REMINDER_RETURN_SHOW = window.chat_conf.intervals.INTERVAL_REMINDER_RETURN_SHOW;

export function interval(from, to) {
    return getRandomInt(from, to);
}

/**
 * Returns a random integer between min (inclusive) and max (inclusive)
 * Using Math.round() will give you a non-uniform distribution!
 */
export function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}