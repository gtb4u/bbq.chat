var chat_bq = {
    init: function () {
        chat_bq.input_focus_color();
        chat_bq.message_radius();
        chat_bq.nice_scroll();
        chat_bq.highest_z_index();
        chat_bq.info_popup();
        chat_bq.form_submit();
        chat_bq.bell_notice();
    },
    operatorStartConnect() {
        $(' .chat-bq .loader-bq').addClass('active-loader-bq');
    },
    operatorIsConnected() {
        $(' .chat-bq .icon-bq').addClass('active-bq');
    },
    operatorIsDisconnected() {
        $(' .chat-bq .icon-bq').removeClass('active-bq');
    },
    operatorEndConnect() {
        $(' .chat-bq .loader-bq').removeClass('active-loader-bq');
    },
    operatorStartWrite: function () {
        $('.write-bq').addClass('active-bq');
        $('.icon-bq').addClass('typing-bq');
    },
    operatorEndWrite: function () {
        $('.write-bq').removeClass('active-bq');
        $('.icon-bq').removeClass('typing-bq');
    },
    operatorHasRead() {
        $('.right-bq:last-child').toggleClass('readed-bq');
    },
    input_focus_color: function () {
        var $input = $('input:not(.submit-bq)');
        $input.on('focusin', chat_bq.focusin).on('focusout', chat_bq.focusout);
    },
    focusin: function () {
        var $form_parent = $('.wrap-form-bq');
        $form_parent.addClass('focused-bq');

        if ($(window).innerWidth() <= 414 || $(window).innerHeight() <= 414) {
            $('.chat-bq').addClass('static');
        } else {
            $('.chat-bq').removeClass('static');
        }
    },
    focusout: function () {
        var $form_parent = $('.wrap-form-bq');
        $form_parent.removeClass('focused-bq');
        if ($(window).innerWidth() <= 414 || $(window).innerHeight() <= 414) {
            $('.chat-bq').removeClass('static');
        }
    },
    vars: function () {
        var blocks = [
            $('.chat-bq'),
            $('.content-bq'),
            $('.top-bq'),
            $('.message-bq'),
            $('.bottom-bq'),
            $('.footer-bq'),
            $('.header-bq'),
            $('.wrap-form-bq'),
            $(window)
        ];

        var blocksInfo = {};

        for (var block in blocks) {

            if (blocks.hasOwnProperty(block)) {
                var key = typeof blocks[block][0]['classList'] !== 'undefined' ? blocks[block][0]['classList'][0] : 'window';

                blocksInfo[key] = {
                    'jq': blocks[block],
                    'height': blocks[block][0].clientHeight,
                    'width': blocks[block][0].clientWidth
                };
            }
        }

        return blocksInfo;
    },
    bind_resize_window: function () {
        var vars = this.vars();
        // vars['top-bq'].jq.css({
        //     'height': vars['content-bq'].height - vars['bottom-bq'].height
        // });
        // vars['message-bq'].jq.css({
        //     'height': vars['content-bq'].height - vars['bottom-bq'].height
        // });
        // vars['content-bq'].jq.bind('DOMSubtreeModified', function () {
        //     vars['message-bq'].jq.css({
        //         'height': vars['content-bq'].height - vars['bottom-bq'].height
        //     });
        // });
        chat_bq.scroll_down();
        if (vars['window'].jq[0].innerWidth <= 414) {
            vars = this.vars();
            vars['header-bq'].height = 58;
            vars['footer-bq'].height = 41;
            vars['chat-bq'].jq.addClass('mobile-bq');
            vars['chat-bq'].jq.removeClass('mobile-landscape-bq').addClass('mobile-portrait-bq');

            vars['footer-bq'].jq.show();
            // vars['content-bq'].jq.css({
            //     'height': vars['window'].jq[0].innerHeight - (vars['header-bq'].height + vars['footer-bq'].height)
            // });
            // vars['top-bq'].jq.css({
            //     'height': vars['window'].jq[0].innerHeight - (vars['header-bq'].height + vars['footer-bq'].height + $('.wrap-form-bq').outerHeight())
            // });
            // vars['message-bq'].jq.css({
            //     'height': vars['window'].jq[0].innerHeight - (vars['header-bq'].height + vars['footer-bq'].height + $('.wrap-form-bq').outerHeight())
            // });
            // vars['content-bq'].jq.bind('DOMSubtreeModified', function () {
            //     vars['message-bq'].jq.css({
            //         'height': vars['window'].jq[0].innerHeight - (vars['header-bq'].height + vars['footer-bq'].height + $('.wrap-form-bq').outerHeight())
            //     });
            // });
            chat_bq.scroll_down();
        } else if (vars['window'].jq[0].innerHeight <= 414) {
            vars = this.vars();
            vars['footer-bq'].height = 0;
            vars['header-bq'].height = 58;
            vars['chat-bq'].jq.removeClass('mobile-portrait-bq').addClass('mobile-landscape-bq');
            vars['chat-bq'].jq.addClass('mobile-bq');
            vars['footer-bq'].jq.hide();
            // vars['content-bq'].jq.css({
            //     'height': vars['window'].jq[0].innerHeight - vars['header-bq'].height
            // });
            // vars['top-bq'].jq.css({
            //     'height': vars['window'].jq[0].innerHeight - (vars['header-bq'].height + $('.wrap-form-bq').outerHeight())
            // });
            // vars['message-bq'].jq.css({
            //     'height': vars['window'].jq[0].innerHeight - (vars['header-bq'].height + $('.wrap-form-bq').outerHeight())
            // });
            // vars['content-bq'].jq.bind('DOMSubtreeModified', function () {
            //     var vars = chat_bq.vars();
            //     vars['message-bq'].jq.css({
            //         'height': vars['window'].jq[0].innerHeight - (vars['header-bq'].height + $('.wrap-form-bq').outerHeight())
            //     });
            // });
            chat_bq.scroll_down();
        }
    },
    content_height: function () {
        chat_bq.bind_resize_window();
    },
    mobile_sizes: function () {
        chat_bq.bind_resize_window();
    },
    message_radius: function () {
        var $message_bq = $('.message-bq');
        $message_bq.bind('DOMSubtreeModified', function () {
            var $last = $('.ms-bq:last-child'),
                height = $last.outerHeight();

            if (height > 42) {
                $last.addClass('alt-bq');
            }
            chat_bq.nice_scroll();
        });
    },
    nice_scroll: function () {
        // $(document).find(".chat-bq .message-bq").niceScroll({
        //     nativeparentscrolling: true,
        //     cursorwidth: "5px",
        //     cursorcolor: "#d4d4d4",
        //     cursorborderradius: "0"
        // });
    },
    highest_z_index: function () {
        var index_highest = 0;
        $("*").each(function () {
            var index_current = parseInt($(this).css("zIndex"), 10);
            if (index_current > index_highest) {
                index_highest = index_current;
            }
        });

        $('.chat-bq').css({
            'z-index': index_highest + 1
        });
        $('.info-popup-bq').css({
            'z-index': index_highest + 4000
        });

    },
    openChat() {
        let $chat = $('.chat-bq');
        $chat.find('.text-bq').blur();
        $('.notice-bq').addClass('hide-bq');
        setTimeout(function () {
            $chat.addClass('nicescroll-visible');
            $chat.find('.text-bq').blur();
        }, 300);
        $chat.removeClass('closed-bq active-popup-greeting-bq active-popup-ask-bq');
        $chat.find('.text-bq').blur();
        //new js
        if ($(window).width() < 767) {
            $('body').css('overflow', 'hidden')
        }
        //end new js
    },
    openGreetings() {
        $('.chat-bq').addClass('active-popup-greeting-bq');
    },
    closeGreetings() {
        $('.chat-bq').removeClass('active-popup-greeting-bq');
    },
    showBell() {
        $('.notice-bq').show();
    },
    hideBell() {
        $('.notice-bq').hide();
    },
    closeChat() {
        let $chat = $('.chat-bq');
        $chat.addClass('closed-bq');
        $chat.removeClass('nicescroll-visible active-popup-greeting-bq active-popup-ask-bq');
        //new js
        if ($(window).width() < 767) {
            $('body').css('overflow', 'visible')
        }
        //end new js
    },
    disableInput: function () {
        $('.msg-form-bq').addClass('disable-msg-bq');
        $('.current-bq').find('.text-bq').blur();
        chat_bq.focusout();
    },
    enableInput: function () {
        $('.msg-form-bq').removeClass('disable-msg-bq');
        chat_bq.focusInput();
    },
    focusInput: function () {
        $('.current-bq').find('.text-bq').focus();
    },
    scroll_down: function () {
        var $message_bq = $(document).find(".chat-bq .message-bq");
        $message_bq.stop(true, true).animate({scrollTop: $message_bq[0].scrollHeight}, 500);
    },
    play_sound: function () {
        var $audio = $('#audio-bq')[0];
        var playSound = function () {
            $audio.currentTime = 0;
            $audio.play();
        };
        playSound();
    },
    info_popup: function () {
        var $info_popup_bq = $('.info-popup-bq');
        $(document).on('click', '.close-info-bq', function () {
            $info_popup_bq.addClass('hidden-bq');
        }).on('click', '.open-pop-bq', function () {
            $info_popup_bq.removeClass('hidden-bq');
        });
        $(document).on('click', function (e) {
            if (!$info_popup_bq.hasClass('hidden-bq') && !$(e.target).hasClass('open-pop-bq') && !$('.info-popup-bq').has(e.target).length > 0) {
                $info_popup_bq.addClass('hidden-bq');
            }
        });
    },
    form_submit: function () {
        var $msg_input = $('.msg-form-bq input:not(.submit-bq)');
        if (localStorage.getItem('not_first_user_message') === '') {
            $msg_input.attr('maxlength', '5000');
        } else {
            $msg_input.attr('maxlength', '2000');
            localStorage.setItem('not_first_user_message', '');
        }
    },
    bell_notice: function () {
        $('.looped-bq').each(function (i) {
            var t = $(this);
            setTimeout(function () {
                t.addClass('active-bq');
                setTimeout(function () {
                    t.removeClass('active-bq');
                    if (i == $('.looped-bq').length - 1) chat_bq.bell_notice();
                }, 5000);
            }, 5000 * i);
        });
    },
    close_greeting: function () {
        $('.chat-bq').removeClass('closed-bq active-popup-greeting-bq');
        setTimeout(function () {
            $(document).find(".chat-bq .msg-form-bq input[type='text']").focus();
        }, 100);
    },

    reset: function () {
        chat_bq.operatorIsDisconnected();
        chat_bq.disableInput();
        chat_bq.hideBell();
    },

    openReminder: function () {
        $('.chat-bq').addClass('active-popup-ask-bq');
    },
    closeReminder: function () {
        $('.chat-bq').removeClass('active-popup-ask-bq');
    },
    openLocation() {
        $('.location-form-bq').addClass('current-bq').siblings('.form-bq').removeClass('current-bq');
        chat_bq.content_height();
        chat_bq.scroll_down();
    },
    openMainInput() {
        $('.msg-form-bq').addClass('current-bq').siblings('.form-bq').removeClass('current-bq');
        chat_bq.content_height();
        chat_bq.scroll_down();
    },
    openPhone() {
        $('.phone-form-bq').addClass('current-bq').siblings('.form-bq').removeClass('current-bq');
        chat_bq.content_height();
        chat_bq.scroll_down();
    },
    openName() {
        $('.name-form-bq').addClass('current-bq').siblings('.form-bq').removeClass('current-bq');
        chat_bq.content_height();
        chat_bq.scroll_down();
    },
    openEmail() {
        $('.email-form-bq').addClass('current-bq').siblings('.form-bq').removeClass('current-bq');
        chat_bq.content_height();
        chat_bq.scroll_down();
    },
    openBudget() {
        $('.budget-form-bq').addClass('current-bq').siblings('.form-bq').removeClass('current-bq');
        chat_bq.content_height();
        chat_bq.scroll_down();
    },
    openDate() {
        $('.period-form-bq').addClass('current-bq').siblings('.form-bq').removeClass('current-bq');
        chat_bq.content_height();
        chat_bq.scroll_down();
    },
    openPhoneErrorMessage() {
        $(document).find('.phone-form-bq .ex-bq-info').removeClass('vis-bq');
        $(document).find('.phone-form-bq .ex-bq-error').addClass('vis-bq');
    },
    closePhoneErrorMessage() {
        $(document).find('.phone-form-bq .ex-bq-error').removeClass('vis-bq');
        $(document).find('.phone-form-bq .ex-bq-info').addClass('vis-bq');
    },
    openEmailErrorMessage() {
        let $email_form = $('.email-form-bq');
        $email_form.addClass('not-valid-bq');
        $email_form.find('.ex-bq-1').addClass('vis-bq');
    },
    closeEmailErrorMessage() {
        let $email_form = $('.email-form-bq');

        $email_form.removeClass('not-valid-bq');
        $email_form.find('.ex-bq-1').removeClass('vis-bq');
    },
    closeNameErrorMessage() {
        var $name_form = $('.name-form-bq');
        $name_form.removeClass('not-valid-bq');
        $name_form.find('.ex-bq-1').removeClass('vis-bq');
    },
    openNameErrorMessage() {
        var $name_form = $('.name-form-bq');
        $name_form.addClass('not-valid-bq');
        $name_form.find('.ex-bq-2').addClass('vis-bq');
    }
};

$(document).ready(function () {
    chat_bq.init();
    $('.chat-bq').next('div').remove();
});

$(window).on('resize', function () {
    chat_bq.mobile_sizes();
}).on('load', function () {
    chat_bq.content_height();
    chat_bq.scroll_down();
    chat_bq.nice_scroll();
});