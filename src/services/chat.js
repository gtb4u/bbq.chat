import listeners from './listeners'
import {OPEN_HELP_POPUP} from "../const";
import {CLEAR_HISTORY} from "../const";
import {CLEAR_INSTANCE} from "../const";
import {HOST_API} from "../const";

export default {
    enableInput() {
        chat_bq.enableInput();
    },
    disableInput() {
        chat_bq.disableInput();
    },
    focusInput(){
        chat_bq.focusInput();
    },
    reset() {
        chat_bq.reset();
    },

    operatorIsConnected() {
        chat_bq.operatorIsConnected();
    },

    scrollDown() {
        chat_bq.scroll_down();
    },

    close() {
        chat_bq.closeChat();
    },

    open() {
        chat_bq.openChat();
    },

    closeReminder() {
        chat_bq.closeReminder();
    },
    operatorHasRead() {
        chat_bq.operatorHasRead();
    },

    closeGreetings() {
        chat_bq.close_greeting();
    },

    openGreetings() {
        chat_bq.openGreetings();
    },

    showBell() {
        chat_bq.showBell();
    },
    hideBell() {
        chat_bq.hideBell();
    },

    operatorEndWrite() {
        chat_bq.operatorEndWrite();
    },

    operatorStartWrite() {
        chat_bq.operatorStartWrite();
    },

    operatorEndConnect() {
        chat_bq.operatorEndConnect();
    },

    operatorStartConnect() {
        chat_bq.operatorStartConnect();
    },

    play() {
        chat_bq.play_sound();
    },

    openReminder: function () {
        chat_bq.openReminder();
    },

    onUnLoad($vm) {
        listeners.addEvent(window, 'unload', function (event) {
            if(event.currentTarget.performance.navigation.type == 1){
                $vm.$store.dispatch(CLEAR_INSTANCE);
            } else {
                $vm.$store.dispatch(CLEAR_HISTORY);
            }
        });
    },
    sendDataToServer($vm){
        if(window.sendTimes === 0 || window.sendTimes === undefined) {
            let data = this.composePostData($vm.collectedData);
            $.ajax({
                url: HOST_API,
                type : 'POST',
                data : data,
                success : function (response) {
                    window.sendTimes++;
                    console.log(response);
                    if(window.Chat_Submit_Success == undefined){
                        console.log('goal worked');
                        window.ym(53445169,'reachGoal', 'Chat_Submit_Success');
                        window.Chat_Submit_Success = true;
                    }
                },
                error : function (responseError) {
                    console.log(responseError);
                }
            });
        }
    },
    onBeforeLoad($vm) {
        let chat = this;
        if($vm.$store.state.instancesCount < 1) {
            if(!$vm.$store.getters.isNotCollectedMinimalData) {
                listeners.addEvent(window, 'beforeunload', function () {
                    chat.sendDataToServer($vm);
                });
            }
        }
    },

    clearPhone(val){
        if(val == '') return val;
        return val.replace(/\D/g,'');
    },

    formatDate(val){
        if(val == '') return val;
        var today = new Date(val);
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return dd + '.' + mm + '.' + yyyy;
    },

    composePostData(collected) {
        let sign = 'e75999117c2d12443622d52d4ddd1972';
        let timestamp = 1544972466;

        const AGENTS = {
            '0ebfbec0': 'Ремонт',//repair
            '39cd64bf': 'Строительство',//building/construction
            '77f6e16f': 'Дизайн',//design
            '9f266731': 'Благоустройство',//landscape
        };

        let wrapperData = {
            "affiliateID": window.referrerParams.a_aid || window.chat_bq_conf.additional.AffiliateID || "bd26de2f",//"{{AffiliateID from PAP - string - required}}",//string,required // параметр из РАР, идентификатор веб-мастера
            "PapvisitorID": window.referrerParams.a_bid || window.chat_bq_conf.additional.PapvisitorID || "1431242_123123",
            "bannerID": window.chat_bq_conf.additional.bid || "a2fd9f79",//"{{bid from PAP - string - required}}",//string,required // параметр из РАР, идентификатор баннера
            "campaignId": window.chat_bq_conf.additional.CampaignID || "39cd64bf",//"{{CampaignID from PAP - string - required}}",//string,required // параметр из РАР, идентификатор кампании
            "channel": window.chat_bq_conf.additional.Channel || '',//"{{Channel from PAP - string}}",//string // параметр из РАР, идентификатор канала
            "formType": AGENTS[window.chat_bq_conf.additional.CampaignID || "39cd64bf"] + ' Chat',//"Form name - string", //string // Название формы. Выводиться в заголовках CRM , например: http://prntscr.com/llkxwq
            "phone": this.clearPhone(collected['phone-number'] || ''),//"{{userphone form form - number - required}}",//string,required //параметр из формы, раздел "телефон" заказчика, например: http://prntscr.com/ll5u8x
            "username": collected.name || '',//"{{username form form - string - required}}",//string,required //параметр из формы, раздел "имя" заказчика, например: 380111111111 (допускаются только цифры)
            "email": collected.email || '',//"{{useremail form form - string (email validation)}}",//strings //параметр из формы, раздел "email" заказчика, например: email@email.com (допускаются валидация в формате email)
            "desc": collected.firstMessage || '',//"{{desc form form - text}}",//string //параметр из формы, описание работ заказчика
            "address": collected.location || '',//"{{address form form - string}}",//string //параметр из формы, раздел "адрес" заказчика. Адрес должен соответствовать выдачи Гугл АПИ (http://prntscr.com/lll37f)
            "price": collected.budget  || 0,//"{{price form form - string}}",//параметр из формы, раздел "бюджет" заказчика. Желательно числовые данные, но допускается строка..
            "date_from": this.formatDate(collected.date || collected['date'] || ''),//"{{date_from form form - date (format: 'd.m.Y', ‘01.01.2018’)}}",//параметр из формы “дата начала” в формате 01.01.2018
            // "date_to": this.formatDate(collected['date-period'] || ''),//"{{date_to form form - date (format: 'd.m.Y', ‘01.01.2018’)}}",//параметр из формы “дата окончания” в формате 01.01.2018
            // "date_until": this.formatDate(collected['date-period1'] || ''),//"{{date_until form form - date (format: 'd.m.Y', ‘01.01.2018’)}}",//параметр из формы “дата не позднее чем” в формате 01.01.2018
            "form_id": 3,//"unique id of form number",//параметр из кода виджета - уникальный номер формы
            "data1": window.chat_bq_conf.additional.Data1 || '',//"{{Data1 from PAP - string}}",//параметр из кода виджета - пользовательское поля аффилиата для PAP
            "data2": window.chat_bq_conf.additional.Data2 || '',//"{{Data2 from PAP - string}}",//параметр из кода виджета - пользовательское поля аффилиата для PAP
            "data3": window.chat_bq_conf.additional.Data3 || '',//"{{Data3 from PAP - string}}",//параметр из кода виджета - пользовательское поля аффилиата для PAP
            "data4": window.chat_bq_conf.additional.Data4 || '',//"{{Data4 from PAP - string}}",//параметр из кода виджета - пользовательское поля аффилиата для PAP
            "data5": window.chat_bq_conf.additional.Data5 || '',//"{{Data5 from PAP - string}}",//параметр из кода виджета - пользовательское поля аффилиата для PAP
            // "consultation": false, //"{{consultation from form - bool}}",//параметр из чата - означает, что заявка имеет консультативный характер
            "sign": sign,//"generated sign* - string",
            "time": timestamp,//"{{timestamp for sign}}"
            "lat": 41.0997803,//"{{timestamp for sign}}"
            "lng": -80.64951940000003,//"{{timestamp for sign}}"
        };

        if(collected.consultation !== undefined){
            wrapperData['consultation'] = 1;
        }

        if(window.chat_bq_conf.additional.CampaignID == '0ebfbec0'){//repair
            if(collected.type_of_flat.length)
                wrapperData['checkbox_480765'] = collected.type_of_flat.join(',');
            if(collected.type_of_object.length)
                wrapperData['select_479713'] = collected.type_of_object.join(',');
            if(collected.type_of_repair.length)
                wrapperData['select_479715'] = collected.type_of_repair.join(',');
        }
        else if(window.chat_bq_conf.additional.CampaignID == '39cd64bf'){//building
            if(collected.type_of_building.length)
                wrapperData['checkbox_479739'] = collected.type_of_building.join(',');
            if(collected.type_of_flat.length)
                wrapperData['checkbox_479737'] = collected.type_of_flat.join(',');
            if(collected.type_of_object.length)
                wrapperData['checkbox_479735'] = collected.type_of_object.join(',');
        }
        else if(window.chat_bq_conf.additional.CampaignID == '77f6e16f'){//design
            if(collected.type_of_service.length)
                wrapperData['checkbox_479757'] = collected.type_of_service.join(',');
            if(collected.type_of_flat.length)
                wrapperData['checkbox_480771'] = collected.type_of_flat.join(',');
        }
        else if(window.chat_bq_conf.additional.CampaignID == '9f266731'){//landscape
            if(collected.type_of_design.length)
                wrapperData['checkbox_479761'] = collected.type_of_design.join(',');
            if(collected.type_of_work.length)
                wrapperData['checkbox_480777'] = collected.type_of_work.join(',');
        }

        return wrapperData;
    },

    //add "on load" page event listeners
    onLoad($vm) {
        listeners.addEvent(window, "load", function (e) {
            listeners.addEvent(document, "mousemove", function handleMouseMove(event) {
                let eventDoc, doc, body;

                event = event || window.event; // IE-ism

                // If pageX/Y aren't available and clientX/Y are,
                // calculate pageX/Y - logic taken from jQuery.
                // (This is to support old IE)
                if (event.pageX == null && event.clientX != null) {
                    eventDoc = (event.target && event.target.ownerDocument) || document;
                    doc = eventDoc.documentElement;
                    body = eventDoc.body;

                    event.pageX = event.clientX +
                        (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
                        (doc && doc.clientLeft || body && body.clientLeft || 0);
                    event.pageY = event.clientY +
                        (doc && doc.scrollTop || body && body.scrollTop || 0) -
                        (doc && doc.clientTop || body && body.clientTop || 0);
                }
                // Use event.pageX / event.pageY here
                if (event.pageY <= 10) {
                    if ($vm.$store.getters.isNotCollectedMinimalData) {
                        $vm.$store.dispatch(OPEN_HELP_POPUP);
                    }
                }
            });
        });
    },

    openLocation() {
        chat_bq.openLocation();
    },
    openMainInput() {
        chat_bq.openMainInput();
    },
    openPhone() {
        chat_bq.openPhone();
    },
    openDate() {
        chat_bq.openDate();
    },
    openBudget() {
        chat_bq.openBudget();
    },
    openEmail() {
        chat_bq.openEmail();
    },
    openName() {
        chat_bq.openName();
    },
    openPhoneErrorMessage() {
        $(document).find('.phone-form-bq .ex-bq-info').removeClass('vis-bq');
        $(document).find('.phone-form-bq .ex-bq-error').addClass('vis-bq');
        // chat_bq.openPhoneErrorMessage();
    },
    closePhoneErrorMessage() {
        $(document).find('.phone-form-bq .ex-bq-error').removeClass('vis-bq');
        $(document).find('.phone-form-bq .ex-bq-info').addClass('vis-bq');
        // chat_bq.closePhoneErrorMessage();
    },
    openEmailErrorMessage() {
        chat_bq.openEmailErrorMessage();
    },
    closeEmailErrorMessage() {
        chat_bq.closeEmailErrorMessage();
    },
    closeNameErrorMessage() {
        chat_bq.closeNameErrorMessage();
    },
    openNameErrorMessage() {
        chat_bq.openNameErrorMessage();
    }
}