import Vue from 'vue'
import VueResource from 'vue-resource'

const VueInputMask = require('vue-inputmask').default;

import App from './App.vue'
import store from './store'

Vue.config.productionTip = false;

Vue.use(VueInputMask);
Vue.use(VueResource);

new Vue({
  store,
  render: h => h(App)
}).$mount('#app');
