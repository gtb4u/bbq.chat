export default {
    time() {
        let time = new Date();
        let hour = time.getHours();
        let minute = time.getMinutes();
        let temp = hour;
        temp += ((minute < 10) ? ':0' : ':') + minute;
        return temp;
    },

    date() {
        let d = new Date();
        let month = d.getMonth() + 1;
        let day = d.getDate();
        return (('' + day).length < 2 ? '0' : '') + day + '.' +
            (('' + month).length < 2 ? '0' : '') + month + '.' +
            d.getFullYear();
    },

    isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    },
    isValidPhoneNumber(phone) {
        var pattern = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/i;
        return pattern.test(phone);
    },
    isValidName(name){
        // let pattern = /[^а-я ]/g;
        let pattern = /^[\u0400-\u04FF ]+$/;
        return pattern.test(name);
    },

    getQueryParams(){
        return (new URL(window.location.href)).searchParams;
        // return url.searchParams;
    },

    get(){
        let data = {};

        this.getQueryParams().forEach(function(value, key){
            data[key] = value;
        });

        return data;
    },

    inlineStyle (left, right, bottom) {
        let styleDom = document.createElement('style');
        styleDom.innerHTML = '' +
            '@media all and (min-width: 768px) {' +
            '.chat-bq {' +
            'left: ' + left + " !important;\n" +
            'right: ' + right + " !important;\n" +
            'bottom: ' + bottom + " !important;\n" +
            'z-index: 9999999 !important;' +
            '}}';

        document.getElementsByTagName('head')[0].appendChild(styleDom);
    },

    currentTime(){
        let date = new Date();
        let year = date.getFullYear();
        let month = date.getMonth() < 9 ? "0" + (date.getMonth()+1) : (date.getMonth()+1);
        let day = date.getDate();
        let hour = date.getHours();
        let minutes = date.getMinutes();
        return year + '/' + month + '/' + day + ' ' + hour + ':'
            + minutes;
    },

    passedMoreThen20Minutes(from){
        let Christmas = new Date();
        let today = new Date(from);
        let diffMs = (Christmas - today); // milliseconds between now & Christmas
        let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
        return diffMins > 10;
        // return diffMins > 1;
    }
}